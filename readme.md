# Gyural 1.8.1

(+ new / # update / - removed)

## What's New (1.8.1)

- Added the trigger: gyu.router.append into router.php (see how to bind hooks into router.php)
- Added the param "count" into filter and filter_array methods into standardObject => $obj->filter(array('type', 1), 'COUNT')
- Added the trigger: gyu.after-libs into index.php after the "standard libs"
- Removed LoadClass('users') from index.php
- Added LoadClass('users') into gyu.after-libs hook.
- Added the version.json field: "logStack" to log the execution stack.
- Setup > added some new fields

## What's new (1.8)

- Updated the standardObject.lib.php, now there's 2 new more methods: filter_array() and filterExecute() and updated the filter(). (compare with a prev version)

## What's new (1.7.3):

- Fixed the lib router.lib.php
- Added gyural.js
- Added mail/smtp lib, for send email by SMTP with PHPMailer (set into version.json "sendmail":"smtp" and configure mail/smtp/conf for setup!)

## What's new (1.7.2):

- Fixed the collection "date"
- Added the DIRECTORY_SEPARATOR instead of self specified '/'
- Anonimous routing (http://wiki.gyural.com/docu/router)

## What's new (1.7.1):

- Fixed the get method of standardObject, and improved the filter limit args.
- Fixed the sdk app, some problems on dev on/off mode :(
- .htaccess modified

## What's new (1.7):

- Introduction of hooks (/funcs/third/hooks.php for info)
- Introduction of MethodBuffer (it will cache the content.)
- New mapping method for router b_map (/router.php for info)
- New gyu_sdk to create app, and debug (dev:1 into version.json to use it.)
- New Api built-in system
- MethodDetection now accept a parameter, it will reply true or false [ex. MethodDetect('get')] or an array [ex. MethodDetect()]
- Fixing of method filter into standardObject(s)
- Changed login method, now with Hooks (/app/users/_/users.ctrl.php for info)
- Improved user security, now with Key@Salt method
- Mail moved as App, and attached to Hook system
- Bug fixes
- New licence to use (license.txt), please read it.
- Removed some unused methods.

* * *

### Create .zip (on mac) without .DS_Store

cd directory
zip -r zipName.zip zipName/ -x *.DS_Store