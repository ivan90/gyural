<?php

/*

----------
Gyural 1.8
----------

Filename: /funcs/third/vars.php
 Version: 1.8
  Author: federicoq <f.quagliotto@mandarinoadv.com>
    Date: 11/10/13

----------------
Collection: Vars
----------------

*/

function vars__get($key, $user = NULL, $complex = NULL) {
	
	if($user == NULL)
		if(logged())
			$user = Me()->id;
	
	$risultati = FetchObject(Database()->query("SELECT * FROM `".tablesPrefix."variables` WHERE `user` = '".$user."' AND `key` = '".$key."' LIMIT 1"));
	
	if(!is_object($risultati))
		return false;
	else if($complex == NULL)
		return $risultati->value;
	else
		return $risultati;

}

function vars__del($key, $user = NULL) {
	
	if($user == NULL)
		if(logged())
			$user = Me()->id;
	
	$risultati = FetchObject(Database()->query("SELECT * FROM `".tablesPrefix."variables` WHERE `user` = '".$user."' AND `key` = '".$key."' LIMIT 1"));
	if(!is_object($risultati))
		return false;
	else {
		return Database()->query("DELETE FROM `".tablesPrefix."variables` WHERE id = '".$risultati->id."'");
	}
}

function vars__set($key, $value, $user = NULL) {
	if($user == NULL) {
		if(logged())
			$user = Me()->id;
	}
	$risultati = FetchObject(Database()->query("SELECT * FROM `".tablesPrefix."variables` WHERE `user` = '".$user."' AND `key` = '".$key."' LIMIT 1"));
	if(!is_object($risultati)) {
		Database()->query("INSERT INTO `".tablesPrefix."variables` SET `user` = '".$user."', `key` = '".$key."', `value` = '".$value."', `date` = '".time()."'");
		return get_var($key, $user);
	}
	else {
		Database()->query("UPDATE `".tablesPrefix."variables` SET `user` = '".$user."', `key` = '".$key."', `value` = '".$value."', `date` = '".time()."' WHERE id = '".$risultati->id."'");
		return CallFunction('vars', 'get', $key, $user);
	}
}

?>