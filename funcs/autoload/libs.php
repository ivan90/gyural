<?php

/*

------------
Gyural 1.8
------------

Filename: /funcs/autoload/libs.php ~ it will be renamed classes.php in next versions.
 Version: 1.8
  Author: federicoq <f.quagliotto@mandarinoadv.com>
    Date: 24/01/2014

-------
Classes
-------

*/

# Load a class. Force the ~/libs/lib.lib.php prefixing * to the $lib
function LoadClass($lib, $istance = false, $args = null) {
	
	deb_log($lib, 'loadClass');

	$info = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, 2);
	
	$file = $info[0]["file"];

	CallFunction('hooks', 'get', 'system.loadclass', $lib, $info, $file); // :gyu.init-libs

	$remember = null;
	if(strstr($file, '.lib')) {
		list($parte) = explode('.lib', $file);
		$porzioni = explode('/', $parte);
		$remember = $porzioni[count($porzioni)-1];
	}

	if($lib[0] != '*') {
		
		if(strstr($lib, '/')) {
			list($parentLib) = explode('/', $lib);
			$nameLib = str_replace('/', '_', $lib);
		} else {
			$parentLib = $nameLib = $lib;
		}
		
		$specific = application . $parentLib . DIRECTORY_SEPARATOR . '_' . DIRECTORY_SEPARATOR . $nameLib.'.lib.php';
		
	}
	else {
		$lib = str_replace('*', '', $lib);
		$specific = libs . $lib . ".lib.php";
		$nameLib = $lib;
	}
	
	$global = libs . $nameLib . ".lib.php";
	if(is_file($specific))
		$whereIsLocated = $specific;
	else if(is_file($global))
		$whereIsLocated = $global;

	if(is_file($whereIsLocated)) {
		
		$content = file_get_contents($whereIsLocated);
		if(strstr($content, '### GYURAL ###')) {
			include_once $whereIsLocated;
			
			if($istance == false)
				return $nameLib;
			else {
				$obj = new $nameLib($args);
				if(method_exists($obj, 'gyu_tablesPrefix'))
					if($obj->gyu_table) $obj->gyu_tablesPrefix();
				return $obj;
			}
			
		}
	}
	
	return 'stdClass';
	
}

function Map($method, $array, $args) {
	if(!is_array($array))
		return 'false';
	else
		foreach($array as $key=>$res) {
			$objectRefreshed = $res;
			$objectRefreshed->$method($args);
			$return[$key] = $objectRefreshed;
		}
	return $return;
}

// alias of Map(…)
function ObjectMap($method, $array) {
	return Map($method, $array);
}

?>