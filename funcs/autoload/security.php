<?php

/*

------------
Gyural 1.8
------------

Filename: /funcs/autoload/security.php
 Version: 1.8
  Author: federicoq <f.quagliotto@mandarinoadv.com>
    Date: 21/11/13

--------
Security
--------

Security functions to validate or invalidate application execution

*/

function Right($m, $ifBad = null) { # In $m there's the lower level for execute the app
	if($m === "*") {
		# Star mean that you'll have at least to be logged...
		
		// For a security reason, if the $m is * we'll set the application level to 99 and the executor to 100.
		$m = 99;
		if(Logged())
			$accessLevel = 100;
		else
			$accessLevel = -1;
	} else {
		// In that case, if the user is not logged, his access level will be set at 0
		if(!isset($_SESSION["login"]))
			$accessLevel = 0;
		else {
			// Here's the real control on applications.
			$debug = debug_backtrace();
			$pagina = $debug[0]["file"];
			list($pagina) = explode("/", str_replace(array(application, '.php'), "", $pagina));
			
			$res = FetchObject(Database()->query("SELECT * FROM `".tablesPrefix."rights` WHERE `group_id` = '".$_SESSION["login"]->group."' AND `application` = '".$pagina."' AND `right` >= $m LIMIT 1"));
			
			if(!is_object($res))
				$accessLevel = -1;
			else
				$accessLevel = $res->right;
		}
	}
	
	if($m > $accessLevel) {
		if($ifBad == null)
			header('Location: '.uriLogin);
			#die('<code>You can\'t call this application. Your access level is too low..<br />Please, contact the Server Administrator.</code>');
			// No way. The user cannot execute this application.
		else
			header("Location: " . $ifBad);
	}
	
}

function i_can($what, $level) {
	$res = FetchObject(Database()->query("SELECT * FROM `".tablesPrefix."rights` WHERE `group_id` = '".Me()->getAttr('group')."' AND `application` = '".$what."' AND `right` >= $level LIMIT 1"));
	if(is_object($res))
		return true;
	else
		return false;
}

function Logged() {
	if(@is_object($_SESSION["login"]) && $_SESSION["login"]->active == 1)
		return true;
	else
		return false;
}

function Me($obj = false) {
	if($obj != false)
		$_SESSION["login"] = $obj;
	if(Logged())
		return $_SESSION["login"];
}

?>