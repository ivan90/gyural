<?php

/*

------------
Gyural 1.8
------------

Filename: /funcs/autoload/database.php
 Version: 1.8
  Author: federicoq <f.quagliotto@mandarinoadv.com>
    Date: 24/01/2014

--------
Database
--------

*/

function ParseDatabase($path, $pattern) {
	preg_match($pattern, $path, $results);
	if($results[1] == "mysql") {
		try {
			return MysqlConnect($results[4], $results[2], $results[3], $results[5]);
		} catch (Exception $e) { __error($e); }
	} else {
		throw new Exception('Database engine not supported');
	}
}

function MysqlConnect($server, $username, $password, $database) {
	LoadClass('MySQLgyu');
	try {
		return(new MySQLgyu($server, $username, $password, $database));
	} catch (Exception $e) { __error($e); }
}

function FetchObject($oggetto, $alwayArray = 0, $classe = 'standardObject', $args = null, $query) {
	if(!is_numeric($alwayArray))
		$classe = $alwayArray;
	$classe = !class_exists($classe) ? LoadClass($classe) : $classe;
	$numeroRisultati = $oggetto->num_rows;
	if($numeroRisultati == 1 && $alwayArray == 0)
		return $oggetto->oggettizza($classe, $args, $query);
	else if($numeroRisultati > 1 || ( $numeroRisultati > 0 && $alwayArray == 1)) {
		while($r = $oggetto->oggettizza($classe, $args, $query))
			$out[] = $r;
		$out[0]->{excludeFieldPrefix . 'first'} = true;
		$out[count($out)-1]->{excludeFieldPrefix . 'last'} = true;
		return $out;
	} else
		return false;
}

function CreateQuery($tipo, $tabella, $campi, $identifier) {

	$prevent = preventDuplicate;

	if($tipo == "0" || $tipo == "INSERT" || $tipo == "I") 
		$type = "INSERT"; 
	else if($tipo == 'SI') {
		$type = "INSERT"; 
		$prevent = false;
	}
	else 
		$type = "UPDATE";

	$porzioni = array();
	if($type == "UPDATE") {
		foreach($campi as $chiave => $valore) {
			if(!strstr($chiave, excludeFieldPrefix))
				$porzioni[] = '`'.$chiave.'` = ' . (!is_int($valore) ? "'".addslashes($valore)."'" : addslashes($valore));
		}
		return 'UPDATE `'.$tabella.'` SET ' . implode(", ", $porzioni) . " WHERE " . $identifier;
	} else if($type == "INSERT") {
		foreach($campi as $chiave => $valore) {
			if(!strstr($chiave, excludeFieldPrefix))
				$porzioni[] = '`'.$chiave.'` = ' . (!is_int($valore) ? "'".addslashes($valore)."'" : addslashes($valore));
		}
		if($prevent == true) {
			global $databaseDriver;
			if(!is_numeric(FetchObject($databaseDriver->query("SELECT * FROM `".$tabella."` WHERE " . implode(" AND ", $porzioni) . " LIMIT 1"))->id))
				$proceed = true;
			else
				$proceed = false;
		} else
			$proceed = true;
		if($proceed == true)
			return 'INSERT INTO `'.$tabella.'` SET ' . implode(", ", $porzioni);
		else
			return false;
	}
}

function Database() {
	global $databaseDriver;
	return $databaseDriver;
}

?>