<?php

### GYURAL ###

/*

----------
Gyural 1.8
----------

Filename: /app/mail/_/mail.ctrl.php
 Version: 1.8
  Author: federicoq <f.quagliotto@mandarinoadv.com>
    Date: 21/11/13

*/

class mailCtrl extends standardController {
	
	var $index_tollerant = false;
	
	function CtrlIndex() {
		
		echo 'mail';
		
	}

	function GetView() {
		if($mail = LoadClass('mail', 1)->get($_GET["id"])) {
			header("Content-type: image/png");
			$mail->setAttr('delivery', time());
			$mail->putExecute();
		} else
			echo 'Ooops?';
	}

	function ApiTest() {
		CallFunction('mail', 'save', mail, 'test', 'test');
	}

}
	
?>