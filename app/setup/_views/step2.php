<?
if(isset($_GET["s"])) {	
	$message = json_decode(base64_decode($_GET["s"]));
	?>
	<div data-alert class="alert">
		<?
		if($message[0] == 0)
			echo '<div>La cartella per gli upload non è scrivibile.</div>';
		
		if($message[1] != NULL)
			echo '<div>Il database ha risposto: ' . $message[1] . '</div>';
		?>
	</div>
	<?
}
?>
<h3 class="subs">Configurazione</h3>
<form method="post" action="/setup/2">

	<input type="hidden" name="ssl" value="0" />
	<input type="hidden" name="dev" value="0" />
	<input type="hidden" name="logStack" value="0" />
	<input type="hidden" name="sendmail" value="php" />


	<label>Nome del sito</label>
	<input required="true" type="text" name="siteName" value="<? echo $_SESSION["prov"]->siteName; ?>" />

	<label>URI</label>
	<input required="true" type="text" name="uri" value="<? echo isset($_SESSION["prov"]->uri) ? $_SESSION["prov"]->uri : 'http://' . $_SERVER["HTTP_HOST"]; ?>" />

	<label>Cartella upload</label>
	<input required="true" type="text" name="upl" value="<? echo isset($_SESSION["prov"]->uploadPath) ? $_SESSION["prov"]->uploadPath : str_replace(absolute, '', upload); ?>" />

	<label>Email</label>
	<input required="true" type="text" name="email" value="<? echo $_SESSION["prov"]->mail; ?>" />

	<label>CDN</label>
	<input required="true" type="text" name="cdn" value="<? echo isset($_SESSION["prov"]->cdn) ? $_SESSION["prov"]->cdn : cdn; ?>" />
			
	<label><input type="checkbox" name="ssl" value="1" <? echo $_SESSION["prov"]->ssl == 1 ? 'checked' : ''; ?>/> Il sito funziona in solo SSL</label><br />
	<label><input type="checkbox" name="sendmail" value="smtp" <? echo $_SESSION["prov"]->sendmail == 'smtp' ? 'checked' : ''; ?>/> Invia e-mail con smtp (/mail/_/mail_smtp_conf.lib.php)</label><br />
	<label><input type="checkbox" name="dev" value="1" <? echo $_SESSION["prov"]->dev == 1 ? 'checked' : ''; ?>/> Abilita modalità sviluppatore (creare ambienti + installare apps)</label><br />
	<label><input type="checkbox" name="logStack" value="1" <? echo $_SESSION["prov"]->logStack == 1 ? 'checked' : ''; ?>/> Salva lo stack di esecuzione (funziona solo in modalità dev)</label><br />
	
	
	<h3 class="subs">Impostazioni del Database</h3>
	<label>Mysql Host</label>
	<input required="true" type="text" name="db_host" value="<? echo isset($_SESSION["db_host"]) ? $_SESSION["db_host"] : localhost; ?>" />

	<label>Mysql Username</label>
	<input required="true" type="text" name="db_user" value="<? echo isset($_SESSION["db_user"]) ? $_SESSION["db_user"] : ''; ?>" />

	<label>Mysql password</label>
	<input required="true" type="text" name="db_pass" value="<? echo isset($_SESSION["db_pass"]) ? $_SESSION["db_pass"] : ''; ?>" />

	<label>Mysql database name</label>
	<input required="true" type="text" name="db_name" value="<? echo isset($_SESSION["db_name"]) ? $_SESSION["db_name"] : ''; ?>" />

	<label>db prefix</label>
	<input type="text" name="db_prefix" value="<? echo isset($_SESSION["db_prefix"]) ? $_SESSION["db_prefix"] : ''; ?>" />
	
	<h3 class="subs">Creazione Primo Utente?</h3>
	<label>Username</label>
	<input required="true" type="text" name="user_username" value="<? echo $_SESSION["user_username"]; ?>" />
	<label>Password</label>
	<input required="true" type="text" name="user_password" value="<? echo $_SESSION["user_password"]; ?>" />
	<label>(Se non vuoi creare un utente base, lascia liberi i campi :)</label>

	<input type="submit" class="button large success" value="Procedi (verifica del db e poi conferma installazione)" style="margin-top: 20px" />
</form>