<?php

### GYURAL ###

class setupCtrl extends standardController {
	
	var $index_tollerant = false;
	var $steps = 3;
	
	# Step 1
	function GetIndex() {
		
		unset($_SESSION["prov"]);
		$app_data["head"]["title"] = 'Setup. 1/' . $this->steps;
		
		Application('setup/_views/_commons/header', null, $app_data);
		Application('setup/_views/step1', null, $app_data);
		Application('setup/_views/_commons/footer');
		
	}
	
	# Step 2
	function Get2() {
		
		$app_data["head"]["title"] = 'Setup. 2/' . $this->steps;
		
		Application('setup/_views/_commons/header', null, $app_data);
		Application('setup/_views/step2', null, $app_data);
		Application('setup/_views/_commons/footer');
		
	}
	
	# Step 2 - Submit
	function Post2() {
		
		$master = json_decode(file_get_contents(absolute . 'sys/version.json'));
		
		$master->working->siteName = $_POST["siteName"];
		$master->working->uri = $_POST["uri"];
		$master->working->ssl = $_POST["ssl"];
		$master->working->cdn = $_POST["cdn"];
		$master->working->mail = $_POST["email"];
		$master->working->supportMail = $_POST["email"];
		$master->working->tablesPrefix = $_POST["db_prefix"];
		$master->working->dev = $_POST["dev"];
		$master->working->sendmail = $_POST["sendmail"];
		$master->working->logStack = $_POST["logStack"];
		$master->working->IndexApp = 'gyu_index/index';
		$master->working->mysql = 'mysql://' . $_POST["db_user"] . ':' . $_POST["db_pass"] . '@' . $_POST["db_host"] . '/' . $_POST["db_name"];
		$master->working->uploadPath = $_POST["upl"];
		
		// Let's check if..
		
		# 1) upload path is writable!
		$uploadPass = is_writable(absolute . $_POST["upl"]);
		
		# 2) the database infos are right!
		$mysqlPass = MysqlConnect($_POST["db_host"], $_POST["db_user"], $_POST["db_pass"], $_POST["db_name"])->connect_error;
		
		# Copy the infos.
		foreach($_POST as $k=>$v) $_SESSION[$k] = $v;
		$_SESSION["prov"] = $master->working;
		
		if(isset($_POST["user_username"])) {
			$_SESSION["user_username"] = $_POST["user_username"];
			$_SESSION["user_password"] = $_POST["user_password"];
		}

		if($uploadPass == true && $mysqlPass == NULL) {
			header('Location: /setup/3');
		} else {
			header('Location: /setup/2/s:' . base64_encode(json_encode(array($uploadPass, $mysqlPass))));
		}
		
	}
	
	# Step 3
	function Get3() {
		
		$app_data["head"]["title"] = 'Setup. 3/' . $this->steps;

		Application('setup/_views/_commons/header', null, $app_data);
		Application('setup/_views/step3', null, $app_data);
		Application('setup/_views/_commons/footer');
		
	}
	
	# Step 3 - Submit
	function Post3() {
		
		$simulate = false;
		
		# Step 1, create the new sys.
		file_put_contents(absolute . 'sys/'.($simulate ? 'test' : 'version').'.json', CallFunction('setup', 'json', stripslashes(json_encode(array('working' => $_SESSION["prov"])))));
		
		# Step 2, preare the sql
		$correctDump = str_replace('TABLE `', 'TABLE `' . $_SESSION["db_prefix"], file_get_contents(application . 'setup/_assets/dump.sql'));
		
		$conn = MysqlConnect($_SESSION["db_host"], $_SESSION["db_user"], $_SESSION["db_pass"], $_SESSION["db_name"]);
		$conn->multi_query($correctDump);

		$_SESSION["skip-1"] = true;

		header('Location: /setup/start');
		
	}

	function GetStart() {

		if(isset($_SESSION["user_username"]) && !isset($_SESSION["skip-1"])) {

			$utenteAlpha = LoadClass('users', 1);
			$utenteAlpha->setAttr('username', $_SESSION["user_username"]);
			$utenteAlpha->setAttr('password', $_SESSION["user_password"]);
			$utenteAlpha->setAttr('active', 1);
			$utenteAlpha->setAttr('group', 1);
			
			$utenteAlpha->hangExecute();

			if($utenteAlpha->id > 0)
				unset($_SESSION["user_username"]);

			session_destroy();

		}

		unset($_SESSION["skip-1"]);

		$app_data["head"]["title"] = 'Installazione Completata';
		
		Application('setup/_views/_commons/header', null, $app_data);
		Application('setup/_views/start');

		if(isset($_SESSION["user_username"])) {
			?>
			<script type="text/javascript">
			$(function() {
				location.reload();
			});
			</script>
			<?
		}

	}

	# Vetrina
	function GetVetrina() {

		$app_data["head"]["title"] = 'Versione Vetrina!';

		Application('setup/_views/_commons/header', null, $app_data);
		Application('setup/_views/vetrina');
		Application('setup/_views/_commons/footer');

	}

}