<?php

function gyu_optimization__js($files) {
	ksort($files);
	$fileName = md5(implode('@', $files));
	$_SESSION["optimization"][$fileName] = $files;
	#return '/o/js/' . base64_encode(implode('$', $files)) . '.js';
	return '/o/js/' . $fileName . '.js';
}

function gyu_optimization__css($files) {
	ksort($files);
	$fileName = md5(implode('@', $files));
	$_SESSION["optimization"][$fileName] = $files;
	#return '/o/less/' . base64_encode(implode('$', $files)) . '.css';
	return '/o/less/'.$fileName.'.css';
}

function gyu_optimization__stripComments($source) {

	LoadClass('gyu_optimization/min');
	return JSMin::minify($source);

}
