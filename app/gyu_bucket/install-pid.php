<?php

isGyu();
MethodStandard();

CallFunction('gyu_bucket', 'rlogged');

Application('gyu_bucket/_commons/header');

list($file, $name) = explode('@', $_GET["p"]);

if(isApplication(base64_decode($name)))
	die('! ' . base64_decode($name) . ' is already installed.');

if(is_file(application . 'gyu_bucket/_tmp/' . $file .'.zip')) {
	echo 'Ok, <em>' . $file . '</em> exists.<br />';
	
	mkdir(application . base64_decode($name));
	
	$zip = new ZipArchive;
	
	$res = $zip->open(application . 'gyu_bucket/_tmp/' . $file . '.zip');
	if ($res === TRUE) {
		// extract it to the path we determined above
		$zip->extractTo(application . base64_decode($name));
		$zip->close();
		$s = 1;
	} else {
		$s = 2;
	}
	
	if($s == 1) {
		echo 'Installed as: <strong>'.base64_decode($name).'</strong>';
		unlink(application  . 'gyu_bucket/_tmp/' . $file .'.zip');
	}
	else
		echo 'Error.';
}