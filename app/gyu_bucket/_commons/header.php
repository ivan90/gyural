<?php
$v = array('red', 'green', 'blue', 'black');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	
	<title>Gyural Bucket</title>
	<style type="text/css" media="screen">
		body, pre {
			font-family: Menlo;
			font-size: 12px;
		}
		
		hr {
			border: none;
			border-bottom: 1px solid <? echo $v[rand(0, count($v))]; ?>;
		}
		
		a {
			color: black;
			text-decoration: none;
		}
		
		a:hover {
			background-color: yellow;
		}
	</style>
</head>

<body>
	<? if(CallFunction('gyu_bucket', 'logged')): ?>
	<h1>
		<a href="/gyu_bucket/">gyural-bucket</a>
		<? echo CallFunction('gyu_bucket', 'logged') ? '<span style="color: orange; font-size: 12px">logged</span>' : ''; ?>
	</h1>
	<p>toolkit for relaxed smokers! works with Gyural >= 1.4</p>
	<hr />
	<? endif; ?>