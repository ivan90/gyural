<?php

/*

----------
Gyural 1.9 
----------

Filename: /index.php
 Version: 1.8
  Author: federicoq <f.quagliotto@mandarinoadv.com>
    Date: 24/01/2014

----------

Here we are.
Have a nice day.

http://gyural.com


asdadas

*/

error_reporting(0);

define('gyu', true);

include_once('config.php');
include_once(absolute . 'funcs/gyural.php');

try {

	gyu_autoload();

	CallFunction('hooks', 'get', 'gyu.created', time()); // :gyu.created
	
	// Init Libs
	CallFunction('hooks', 'get', 'gyu.init-libs'); // :gyu.init-libs
	LoadClass('standardObject');		// Main Gyural Object Libs. (http://gyural.com/docs/standardObject)
	LoadClass('standardController');	// Controllers (http://gyural.com/docs/standardController)

	CallFunction('hooks', 'get', 'gyu.after-libs'); // :gyu.after-libs

	// Database Connection
	defined('dbLink') ? $databaseDriver = ParseDatabase(dbLink, $dbLink_pattern) : null;
	
	// Sessions (don't move it)
	session_start();
	CallFunction('hooks', 'get', 'gyu.engine.start', time());
	
	// What version? Showcase or Fully-armed?
	if(db == 1) is_object(Database()) ? CallFunction('hooks', 'get', 'gyu.db.ready') : die('Did you hear the explosion?');
	
	/*
	
	-------
	Runtime
	-------
	
	*/
	
	$env = Route($page);

	$env->exec();

	if(isset($GLOBALS["gyu_buffer"]) && is_array($GLOBALS["gyu_buffer"])) ob_end_flush();
	
	CallFunction('hooks', 'get', 'gyu.engine.off', time());
	if(is_object(Database()))
		Database()->close();
	
	
	CallFunction('hooks', 'get', 'gyu.off', time()); // :gyu.off
	
	if(dev)
		CallFunction('hooks', 'get', 'gyu.dev-zone', time(), $env); // :gyu.dev-zone
	else
		CallFunction('hooks', 'get', 'gyu.dev-zone-hidden', time()); // :gyu.dev-zone-hidden
	
} catch (Exception $e) {
	__error($e);
}

?>